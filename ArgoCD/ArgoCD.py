import configparser
import subprocess
import shutil
import json
import stat
import os

# -----------------
# BITBUCKET DETAILS
# -----------------
bb_user = "akshay466"
bb_pwd = "ksnDYst3ehMpewS38Vwh"
bb_url = "https://api.bitbucket.org/2.0/repositories/akshay466"
repo_name = "employeemicroservices_helm_implementation"
new_branch = "feature/imageupdate"
source_branch = "master"



# -------------------------
# PRE-EXECUTION PREPARATION
# -------------------------
base_dir = os.getcwd()
DETACHED_PROCESS = 0x00000008


# -------------------
# NEW BRANCH CREATION
# -------------------
cmd = 'curl --url "' + bb_url +"/"+ repo_name + '/refs/branches" --user ' + bb_user + ':' + bb_pwd + ' --request POST --header "Content-Type: application/json" --data ' + open(os.path.join(base_dir, "branch_creation.json"), "r").readline().replace("{BRANCH_NAME}", new_branch).replace("{SOURCE_BRANCH}", source_branch)
output = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, creationflags=DETACHED_PROCESS).communicate()
print(output)


# ----------------
# UPDATE IMAGE TAG
# ----------------
cmd = 'git pull'
output = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, creationflags=DETACHED_PROCESS).communicate()
print(output)
cmd = 'git checkout feature/imageupdate'
output = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, creationflags=DETACHED_PROCESS).communicate()
print(output)

# --------------------
# PULLREQUEST CREATION
# --------------------
cmd = 'curl --url "' + bb_url +"/"+ repo_name + '/pullrequests" --user ' + bb_user + ':' + bb_pwd + ' --request POST --header "Content-Type: application/json" --data ' + open(os.path.join(base_dir, "pull_request.json"), "r").readline().replace("{BRANCH_NAME}", new_branch).replace("{SOURCE_BRANCH}", source_branch)
output = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, creationflags=DETACHED_PROCESS).communicate()
print(output)