﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeMicroservices
{
    public class Employee
    {
        public int Id { get; internal set; }
        public string Name { get; internal set; }
        public string Gender { get; internal set; }
        public double Salary { get; internal set; }
    }
}
